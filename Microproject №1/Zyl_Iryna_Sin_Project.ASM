format PE console
entry start

include 'win32a.inc'


section '.data' data readable writable
    msg_input db "Enter x (radians) - the parameter of the sinus function: ", 13, 10, ">> ", 0
    msg_wrong_input db "Bad x, it should be valid float number, try again: ", 13, 10, ">> ", 0

    format_double db "%lf", 0


    msg_step db "power = %d, x^power = %f, factorial = %.2f, sin = %.10f", 13, 10, 0
    msg_output db      "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", 13, 10,\
                       "             SIN(X) = %.10f", 13, 10,\
                       "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", 13, 10, 0
    msg_input_commit db 13, 10, "x = %f", 13, 10, 0
    msg_modul db 13, 10, "x %% pi = %f", 13, 10, 13, 10, 0

    x dq ?
    base dq ? ; x in some power

    sin dq 0.000
    old dq ? ; absolute value of the previous sinus

    fault dq 0.0005

    power dd 1 ; current power of the series (needed for loop)
    factorial dq 1.00 ; facotial for current power

    while_stack dd ?
    step_stack dd ?
    odd_stack dd ?
    inc_stack dd ?
    tmp_esp dd ? ; needed for saving current stack ptr

    buf rd 100

    nan dq ?
    minus_one dq -1.00
    zero dd 0
    nan_flag dd 12549 ; EFL when we have done fxam and sahf from +NAN

;------------------------Code of the program:

section '.code' code readable executable

start:

    call init_fpu

    call read_x

    fld [x]
    fstp [base]
    wait

    call while_sin_is_bad

    ; printing sin
    invoke printf, msg_output, dword[sin], dword[sin+4]

    invoke getch

    ; ending program
    invoke ExitProcess, 0


;--------------------------Subprograms (methods)
; Count sinus step by step while the error still too large
while_sin_is_bad:
    mov [while_stack], esp

    while_begin:
        invoke printf, msg_step, [power], dword[base], dword[base+4],\
        dword[factorial], dword[factorial+4], dword[sin], dword[sin+4]

        call next_sin
        call count_error
        ja while_begin

    mov esp, [while_stack]
    ret

; Compares the difference between the last step and the permissible error size JA, JB, JZ
count_error:
    fld [sin]
    fsub [old]
    fabs
    fld [old]
    fabs
    fstp [old]
    fdiv [old]

    fcomp [fault]
    fstsw ax
    wait

    sahf

    ret

; Counts the next sinus depending on the parity of the power
next_sin:
    mov [step_stack], esp

    mov eax, [power]
    test eax, 1

    jz even_pow
       call odd_pow

    even_pow:
    call increase

    mov esp, [step_stack]
    ret

; Counts the next series term if the power is odd
odd_pow:
    mov [odd_stack], esp

    fld [sin]
    fstp [old]

    fld [base]
    fdiv [factorial]
    fadd [sin]
    fstp [sin]

    fld [base]
    fmul [minus_one]
    fstp [base]
    wait

    mov esp, [odd_stack]
    ret

; Power and factorial increment
increase:
    mov [inc_stack], esp

    mov eax, [power]
    inc eax
    mov [power], eax

    fld [factorial]
    fimul [power]
    fstp [factorial]

    fld [base]
    fmul [x]
    fstp [base]
    wait

    mov esp, [inc_stack]
    ret

;------------------------Methods for the program beginning

; Initialize FPU and constants
init_fpu:
    finit ; FPU Initialization

    fld1
    fdiv [zero] ; 1 / 0 = +oo
    fabs

    fmul [zero] ; +oo * 0 = +NAN
    fstp [nan]
    wait

    ret

; Checks whether st(0) is not a NAN, JZ if NAN, JNZ otherwise
check_none:
    fabs
    fxam
    fstsw ax
    wait

    and eax, [nan_flag]
    cmp eax, [nan_flag]

    ret

; Gets x from input
read_x:
    mov [tmp_esp], esp

    fld [nan]
    fstp [x]

    invoke printf, msg_input
    invoke gets, buf
    invoke sscanf, buf, format_double, x

    fld [x]
    call check_none

    je error

finish_reading:

    invoke printf, msg_input_commit, dword[x], dword[x+4]

    fldpi
    fld [x]
    fprem

    fstp [x]

    invoke printf, msg_modul, dword[x], dword[x+4]

    mov esp, [tmp_esp]
    ret

error:
    mov eax, 0
    mov [buf], eax
    invoke printf, msg_wrong_input
    invoke gets, buf
    invoke sscanf, buf, format_double, x

    fld [x]
    call check_none
    fstp [x]

    je error
    jmp finish_reading


;------------------------------Includes
section '.idata' import data readable
    library kernel, 'kernel32.dll',\
            msvcrt, 'msvcrt.dll',\
            user32,'USER32.DLL'

include 'api\user32.inc'
include 'api\kernel32.inc'
    import kernel,\
           ExitProcess, 'ExitProcess',\
           HeapCreate,'HeapCreate',\
           HeapAlloc,'HeapAlloc'
    include 'api\kernel32.inc'
    import msvcrt,\
           printf, 'printf',\
           sprintf, 'sprintf',\
           sscanf, 'sscanf',\
           scanf, 'scanf',\
           getch, '_getch',\
           puts, 'puts',\
           getchar, 'getchar',\
           fflush, 'fflush',\
           gets, 'gets'