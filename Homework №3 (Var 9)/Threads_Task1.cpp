﻿#include <iostream>
#include<string>
#include<vector>
#include <set>
#include<thread>
#include <memory>
#include <ctype.h>



bool is_union = false, is_intersetion = false, is_difference_ab = false, is_difference_ba = false;

std::vector<std::string> Split(const std::string& string, const std::string& delimiter = " ") {
    int left = 0;
    std::vector<std::string> ans;

    if (string == delimiter) {
        ans.push_back("");
        ans.push_back("");
        return ans;
    }

    if (left >= string.size()) {
        ans.push_back("");
    }
    while (left < string.size()) {
        int pos = string.find(delimiter, left);
        if (pos != std::string::npos) {
            if (left == pos) {
                ans.push_back("");
            }
            else {
                ans.push_back(string.substr(left, pos - left));
                if (pos + delimiter.length() - 1 == string.size() - 1) {
                    ans.push_back("");
                }
            }
            left = pos + delimiter.length();
        }
        else {
            ans.push_back(string.substr(left));
            break;
        }
    }

    std::vector<std::string> result;
    for (auto elem : ans) {
        if (elem.size() > 0) {
            result.push_back(elem);
        }
    }

    return result;
}

bool TryInputSet(std::set<uint64_t>* s) {
    uint64_t num;
    s->clear();
    std::string input;
    getline(std::cin, input);
    std::vector<std::string> nums = Split(input);

    for (size_t i = 0; i < nums.size(); i++) {
        uint64_t num = 0;

        if(nums[i].size() < 1){
            continue;
        }

        if (nums[i][0] == '0' || nums[i].size() > 64) {
            return false;
        }

        for (auto c : nums[i]) {
            if (!isdigit(c)) {
                return false;
            }
            else {
                num *= 10;
                num += c - '0';
            }
        }

        s->insert(num);
    }

    return true;
}

void FillSet(const std::string& message, std::set<uint64_t>* s) {
    std::cout << message;

    while (!TryInputSet(s)) {
        std::cout << "Incorrect input. It must be positive numbers. Try again. " << std::endl;
        std::cout << message;
    }
}

void Union(const std::set<uint64_t>& a, const std::set<uint64_t>& b, const std::set<uint64_t>& c) {
    for (auto elem : a) {
        std::cout << "Union testing. \n";

        if (c.count(elem) == 0) {
            return;
        }
    }

    for (auto elem : b) {
        std::cout << "Union testing. \n";

        if (c.count(elem) == 0) {
            return;
        }
    }

    is_union = true;
}

void Intersection(const std::set<uint64_t>& a, const std::set<uint64_t>& b, const std::set<uint64_t>& c) {
    for (auto elem : a) {
        std::cout << "Intersection testing. \n";

        if ((b.count(elem) != 0 && c.count(elem) == 0) || (b.count(elem) == 0 && c.count(elem) != 0)) {
            return;
        }
    }

    for (auto elem : c) {
        std::cout << "Intersection testing. \n";
        if (a.count(elem) == 0 || b.count(elem) == 0) {
            return;
        }
    }

    is_intersetion = true;
}

void Difference(int flag, const std::set<uint64_t>& a, const std::set<uint64_t>& b,
    const std::set<uint64_t>& c){

    if (a.size() == 0 && c.size() == 0) {
        if (flag == 1) {
            is_difference_ab = true;
        }
        else {
            is_difference_ba = true;
        }

        return;
    }

    for (auto elem : a) {
        std::cout << (flag == 1 ? "Difference A\\B testing. \n" : "Difference B\\A testing. \n");

        if ((b.count(elem) == 0 && c.count(elem) == 0) || (b.count(elem) != 0 && c.count(elem) != 0)) {
            return;
        }
    }

    for (auto elem : c) {
        std::cout << (flag == 1 ? "Difference A\\B testing. \n" : "Difference B\\A testing. \n");
        if (a.count(elem) == 0) {
            return;
        }
    }

    if (flag == 1) {
        is_difference_ab = true;
    }
    else {
        is_difference_ba = true;
    }
}

int main() {
    std::set<uint64_t> a, b, c;

    std::cout << "----------Inserting sets. " << std::endl;
    FillSet("Input set A: ", &a);
    FillSet("Input set B: ", &b);
    FillSet("Input set C: ", &c);

    std::cout << std::endl << "----------Threads creating." << std::endl;
    std::thread thread1(Union, std::ref(a), std::ref(b), std::ref(c));
    std::thread thread2(Intersection, std::ref(a), std::ref(b), std::ref(c));
    std::thread thread3(Difference, 1, std::ref(a), std::ref(b), std::ref(c));
    std::thread thread4(Difference, 2, std::ref(b), std::ref(a), std::ref(c));

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();

    std::cout << std::endl << "----------Results." << std::endl;
    std::cout << "Is C a union of A and B? ----- " << (is_union ? "YES" : "NO") << std::endl;
    std::cout << "Is C an intersection of A and B? ----- " << (is_intersetion ? "YES" : "NO") << std::endl;
    std::cout << "Is C a difference of A and B (A\\B)? ----- " << 
        (is_difference_ab ? "YES" : "NO") << std::endl;
    std::cout << "Is C a difference of B and A (B\\A)? ----- " << 
        (is_difference_ba ? "YES" : "NO") << std::endl;

    return 0;
}