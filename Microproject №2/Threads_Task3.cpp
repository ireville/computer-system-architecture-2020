﻿#include<iostream>
#include<string>
#include<vector>
#include<thread>
#include<mutex>
#include<cstdlib>

/**
 * DB structure.
 */
struct DB {
    int readers_in = 0; // number of readers reading db at the moment.
    int writers_in = 0; // number of writers writing to db at the moment.
    std::vector<std::string> data; // data of the db.
    std::mutex db_mutex;
};

DB db; // our instance of the db.
const int users_cnt = 10; // number of readers and writers.
const int writers_percent = 4; // special number, which regulates percent of writers between all processes (it will be 1/writers_percent)
const int db_size = 20; // size of the data in db.

/**
 * Implements the Read method of the reader.
 * @param user_num Reader number.
 * @param index Index of the db data that is going to be read.
 * @return Read data.
 */
std::string Read(int user_num, int index) {
    std::this_thread::sleep_for(std::chrono::microseconds(1000));

    db.db_mutex.lock();
    db.readers_in++;
    db.db_mutex.unlock();

    std::string info;
    std::string log = "Reader " + std::to_string(user_num) + "\tstarted reading...\n";

    if (index >= 0 && index < db.data.size()) {
        std::cout << log;
        info = db.data[index];
    }

    db.db_mutex.lock();
    db.readers_in--;
    db.db_mutex.unlock();

    log = "Reader " + std::to_string(user_num) + "\tread: " + info + "\n";
    std::cout << log;

    return info;
}

/**
 * Implements the Write method of the writer.
 * @param user_num Writer number.
 * @param index Index of the db data that is going to be rewrited.
 * @param info New value that os going to be written.
 * @return True if writed successfully, false - otherwise.
 */
bool Write(int user_num, int index, const std::string& info) {
    std::this_thread::sleep_for(std::chrono::microseconds(500));

    if (index < 0 || index >= db.data.size()) {
        std::cout << "Error in writing. Incorrect index. ";
        return false;
    }

    std::string log = "Writer " + std::to_string(user_num) + "\tis waiting for writing...\n";

    db.db_mutex.lock();

    while (db.writers_in != 0 || db.readers_in != 0) {
        db.db_mutex.unlock();

        std::cout << log;
        std::this_thread::sleep_for(std::chrono::microseconds(200));
        std::this_thread::yield();

        db.db_mutex.lock();
    }

    db.writers_in++;
    db.db_mutex.unlock();

    log = "Writer " + std::to_string(user_num) + "\tstarted writing...\n";
    std::cout << log;

    std::this_thread::sleep_for(std::chrono::microseconds(500));

    db.data[index] = info;

    log = "Writer " + std::to_string(user_num) + "\tsuccessfully writed \"" + info + "\".\n";
    std::cout << log;

    db.db_mutex.lock();
    db.writers_in--;
    db.db_mutex.unlock();


    return true;
}

/**
 * Generating random string of the given length.
 * @param len Length of the string.
 * @return Randomly generated string.
 */
std::string Generate(int len) {

    std::string tmp;
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    tmp.reserve(len);

    for (int i = 0; i < len; ++i)
        tmp += alphanum[rand() % (sizeof(alphanum) - 1)];


    return tmp;
}

/**
 * Filling the DB with values that equals indexes + 'a' 5 times.
 */
void FillDB() {
    db.data = std::vector<std::string>();

    for (int i = 0; i < db_size; i++) {
        db.data.emplace_back(std::string(5, ('a' + i)));
    }
}

int main() {
    FillDB(); // Filling the DB.

    std::thread threads[users_cnt];

    for (int i = 0; i < users_cnt; i++) {
        int index = rand() % db_size; // randomly chosen data index in db.

        if (rand() % writers_percent == 0) {
            threads[i] = std::thread(Write, i, index, Generate(rand() % 10 + 1));
        }
        else {
            threads[i] = std::thread(Read, i, index);
        }
    }

    for (int i = 0; i < users_cnt; i++) {
        threads[i].join();
    }

    return 0;
}